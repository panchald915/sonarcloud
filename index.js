function greet(name) {
    return "Hello, " + name;
}
var message = greet("Alice"); // Error: Type 'string' is not assignable to type 'number'
console.log(message);
